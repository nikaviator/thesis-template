\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}INTRODUCTION}{4}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Objective Of Thesis:}{4}{paragraph*.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Robots and Cobots}{4}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Components Of a Typical Robot}{6}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Uses And Applied Fields}{7}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Elderly Care}{7}{subsection.1.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Medical Uses}{7}{subsection.1.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Warehouse Operator}{7}{subsection.1.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.4}Food Home Delivery}{8}{subsection.1.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.5}Automotive Industry}{8}{subsection.1.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Importance Of Robots In Today's Time}{8}{section.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Expectations From Robots}{9}{section.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.6}Evolution - Robots to Cobots}{9}{section.1.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}INSPIRATION AND DRIVING FORCE}{10}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}BACKGROUND}{12}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}About Franka Emika Panda Robot}{12}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Robotics - Features}{13}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Sensitivity}{13}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Drive a.k.a Motion}{14}{subsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Impedance}{14}{subsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Collision Detection and Reaction}{14}{subsection.3.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Software Tools Robot Franka Uses}{15}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Robotic Coexistence With Humans - Meaning Of Cobots}{15}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Existence Alongside Humans}{16}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Collaboration and Co-operation}{16}{subsection.3.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Real-time and Presence Acknowledged Collaboration}{16}{subsection.3.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Accidents Due To Malfunctions and Consequences}{16}{section.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Mechanical Failure}{17}{subsection.3.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.2}Electrical Anomaly In Components}{17}{subsection.3.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.3}Malfunctioning Software}{17}{subsection.3.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.4}Human Operator Errors}{17}{subsection.3.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Making Robots Safer And Safe Deployment Practices}{18}{section.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}STATE OF THE ART}{20}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Motion Planning And Simulations}{20}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Modeling - Explaining Choice of Design Depictions}{20}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Unified Modeling Language (UML) Diagram For World Model Class Diagram}{21}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Business Process Modeling Notation (BPMN) for Application Model}{23}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Unified Modeling Language State Machine for Safety Model}{25}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Tools Used}{26}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Setup Environment Using ROS To Run Services, Motion Planning In MOVEIT and GAZEBO For Simulations and more}{26}{subsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}CONCEPT}{32}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Theme}{32}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}The Models Designed}{33}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Programming Of Hardware And Software Components - Purpose And Concepts}{37}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Refactoring and Reclassification}{38}{section.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Connecting Everything - Simulations\\}{38}{section.5.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}IMPLEMENTATION}{40}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Introduction}{40}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}WORLD MODEL IMPLEMENTATION}{41}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.1}Programming - The Implementation Of World Model}{42}{subsection.6.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Future Work Implementing Application Model Programmatically\\}{45}{section.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}EVALUVATION}{46}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.0.1}World Model Implementation Results}{46}{subsection.7.0.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}World Model Test Implementation}{46}{section.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{48}{chapter*.25}%
\defcounter {refsection}{0}\relax 
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
